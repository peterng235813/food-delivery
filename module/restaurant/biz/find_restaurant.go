package restaurantbiz

import (
	"context"
	restaurantmodel "g-5-food-delivery/module/restaurant/model"
)

type FindRestaurantStore interface {
	FindRestaurant(context context.Context, data *restaurantmodel.Restaurant) error
}

type findRestaurantByIdBiz struct {
	store FindRestaurantStore
}

func NewFindRestaurantByIdBiz(store FindRestaurantStore) *findRestaurantByIdBiz {

}

func (biz *findRestaurantByIdBiz) FindRestaurantById(context context.Context, data *restaurantmodel.Restaurant) error {
	if err := biz.store.CreateRestaurant(context, data); err != nil {
		return err
	}

	return nil
}
