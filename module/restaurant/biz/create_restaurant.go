package restaurantbiz

import (
	"context"
	"errors"
	restaurantmodel "g-5-food-delivery/module/restaurant/model"
)

// Interface khai bao o noi dung no. Golang
type CreateRestaurantStore interface {
	CreateRestaurant(context context.Context, data *restaurantmodel.RestaurantCreate) error
}

type createRestaurantBiz struct {
	store CreateRestaurantStore
}

func NewCreateRestaurantBiz(store CreateRestaurantStore) *createRestaurantBiz {
	return &createRestaurantBiz{store: store}
}

func (biz *createRestaurantBiz) CreateRestaurant(context context.Context, data *restaurantmodel.RestaurantCreate) error {
	if data.Name == "" {
		return errors.New("Name can not empty")
	}

	if err := biz.store.CreateRestaurant(context, data); err != nil {
		return err
	}

	return nil
}
